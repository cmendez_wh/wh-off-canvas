/*
 * WhOffCanvas  v1.0
 */

(function($) {

    /**
     * Default options
     * @type {{panelActive: string}}
     */
    var defaults = {
        panelActive: 'main',        // left, main, right
        noTransition: false,        // true, false
        transitionFunction: '',     // ease, linear, ease-in, ease-out, ease-in-out,
        transitionDuration: '',     // slower, slow, fast, faster,
        touch: {
            'swipe': {
                'threshold': 10,    // Minimal distance required before recognizing.
                'velocity':  0.1    // Minimal velocity required before recognizing, unit is in px per ms.
            }
        }
    };

    var methods = {

        /**
         * WhOffCanvas constructor
         * @param options
         */
        init: function(options) {
            var $this = $(this),
                settings = $.extend({}, defaults, options || {});

            // Set settings
            $this.data('settings', settings);

            // Bind events
            methods.bindEvents($this);

            // Set Panel Active
            if($this.data('settings').panelActive !== 'main') {
                methods.setPanel($(this), $this.data('settings').panelActive);
            }

            // Set No Transition
            if($this.data('settings').noTransition === true) {
                methods.disableTranstition($(this));
            }

            // Set Transition Function
            if($this.data('settings').transitionFunction !== '') {
                methods.setTransitionFunction($(this), $this.data('settings').transitionFunction);
            }

            // Set Transition Duration
            if($this.data('settings').transitionDuration !== '') {
                methods.setTransitionDuration($(this), $this.data('settings').transitionDuration);
            }

            // set touch events
            methods.setTouchEvents($(this), $this.data('settings'))
        },

        /**
         * Set Hammer touch eventos to the elements
         * @param element
         * @param settings
         */
        setTouchEvents: function (element, settings) {
            var leftPanelObj,
                rightPanelObj,
                mainObj,
                leftPanelHammer,
                rightPanelHammer,
                mainHammer;

            // get panels elements
            leftPanelObj = element.find('.off-canvas__left');
            rightPanelObj = element.find('.off-canvas__right');
            mainPanelObj = element.find('.off-canvas__main');

            // Main Panel
            if (leftPanelObj.length > 0 || rightPanelObj.length > 0) {
                // Main panel
                mainHammer = new Hammer.Manager(mainPanelObj[0]);
                mainHammer.add(new Hammer.Swipe({
                    threshold: settings.touch.swipe.threshold,
                    velocity: settings.touch.swipe.velocity
                }));
                if (leftPanelObj.length > 0) {
                    mainHammer.on("swiperight", function (ev) {
                        methods.showLeftPanel(element);
                    });
                }
                if (rightPanelObj.length > 0) {
                    mainHammer.on("swipeleft", function (ev) {
                        methods.showRightPanel(element);
                    });
                }
            }

            if (leftPanelObj.length > 0) {
                // Left Panel
                leftPanelHammer = new Hammer.Manager(leftPanelObj[0]);
                leftPanelHammer.add(new Hammer.Swipe({
                    threshold: settings.touch.swipe.threshold,
                    velocity: settings.touch.swipe.velocity
                }));
                leftPanelHammer.on("swipeleft", function (ev) {
                    methods.hideLeftPanel(element);
                });
            }

            // If Right Panel exists
            if (rightPanelObj.length > 0) {
                // Left Panel
                rightPanelHammer = new Hammer.Manager(rightPanelObj[0]);
                rightPanelHammer.add(new Hammer.Swipe({
                    threshold: settings.touch.swipe.threshold,
                    velocity: settings.touch.swipe.velocity
                }));
                rightPanelHammer.on("swiperight", function (ev) {
                    methods.hideRightPanel(element);
                });
            }

        },

        /**
         * Show Panel
         * @param element
         * @param panel
         */
        setPanel: function (element, panel) {
            if (panel === 'left') {
                methods.showLeftPanel(element);
            } else if (panel === 'right') {
                methods.showRightPanel(element);
            }
        },

        /**
         * Disable transition
         * @param element
         */
        disableTranstition: function (element) {
            element.addClass('off-canvas--no-transition');
        },

        /**
         * Set transition function
         * @param element
         * @param transitionFunction
         */
        setTransitionFunction: function (element, transitionFunction) {
            element.addClass('off-canvas--function-' + transitionFunction);
        },

        /**
         * Set transition duration
         * @param element
         * @param transitionDuration
         */
        setTransitionDuration: function (element, transitionDuration) {
            element.addClass('off-canvas--' + transitionDuration);
        },

        /**
         * Bind events
         * @param target
         */
        bindEvents: function (target) {

            target.find('.off-canvas__left-toggle').click(function () {
                methods.showLeftPanel($(this));
            });

            target.find('.off-canvas__left-exit').click(function () {
                methods.hideLeftPanel($(this));
            });

            target.find('.off-canvas__right-toggle').click(function () {
                methods.showRightPanel($(this));
            });

            target.find('.off-canvas__right-exit').click(function () {
                methods.hideRightPanel($(this));
            });

        },

        /**
         * Toggle Left Panel
         * @param element
         */
        showLeftPanel: function (element) {
            var offCanvasInnerWrapper = element.closest('.off-canvas__wrapper');
            offCanvasInnerWrapper.addClass('off-canvas--move-right');
        },

        /**
         * Toggle Right Panel
         * @param element
         */
        showRightPanel: function (element) {
            var offCanvasInnerWrapper = element.closest('.off-canvas__wrapper');
            offCanvasInnerWrapper.addClass('off-canvas--move-left');
        },

        hideLeftPanel: function (element) {
            var offCanvasInnerWrapper = element.closest('.off-canvas__wrapper');
            offCanvasInnerWrapper.removeClass('off-canvas--move-right');
        },

        hideRightPanel: function (element) {
            var offCanvasInnerWrapper = element.closest('.off-canvas__wrapper');
            offCanvasInnerWrapper.removeClass("off-canvas--move-left");
        }

    };

    /**
     * Create whOffCanvas
     * @param method
     * @returns {*}
     */
    $.fn.whOffCanvas = function(method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.WhOffCanvas');
        }
    };

})(jQuery);